from models.book import Book
from services.book_service import BookService

class Library:
    """Klasa reprezentująca bibliotekę."""

    def __init__(self):
        """Inicjalizuje instancję klasy Library i tworzy instancję klasy BookService."""
        self.book_service = BookService()

    def get_all_books(self):
        """Pobiera wszystkie książki z bazy danych.

        Returns:
            list: Lista zawierająca wszystkie książki.
        """
        books = self.book_service.get_all_books()
        return books

    def get_book(self, book_id):
        """Pobiera informacje o książce o podanym identyfikatorze.

        Args:
            book_id (int): Identyfikator książki.

        Returns:
            dict: Informacje o książce w formacie słownika.
        """
        book = self.book_service.get_book(book_id)
        if book:
            book_dict = {
                'id': book[0],
                'title': book[1],
                'author': book[2],
                'year': book[3]
            }
            return book_dict
        else:
            return {}

    def add_book(self, book_data):
        """Dodaje nową książkę do biblioteki.

        Args:
            book_data (dict): Informacje o książce.

        Returns:
            dict: Identyfikator dodanej książki w formacie słownika.
        """
        title = book_data.get('title')
        author = book_data.get('author')
        year = book_data.get('year')

        if not title or not author or not year:
            return {}

        book = Book(title, author, year)
        book_id = self.book_service.add_book(book)

        return {'book_id': book_id}

    def update_book(self, book_id, book_data):
        """Aktualizuje informacje o książce o podanym identyfikatorze.

        Args:
            book_id (int): Identyfikator książki.
            book_data (dict): Zaktualizowane informacje o książce.

        Returns:
            dict: Pusty słownik.
        """
        book = self.get_book(book_id)
        if not book:
            return {}

        updated_book = Book(book_data.get('title', book['title']),
                            book_data.get('author', book['author']),
                            book_data.get('year', book['year']))
        self.book_service.update_book(book_id, updated_book)

        return {}

    def delete_book(self, book_id):
        """Usuwa książkę o podanym identyfikatorze.

        Args:
            book_id (int): Identyfikator książki.

        Returns:
            dict: Informacje o usuniętej książce w formacie słownika.
        """
        book = self.get_book(book_id)
        if not book:
            return {}
        self.book_service.delete_book(book_id)
        return book
