=========================================
Books Manager Dokumentacja
=========================================
Dokumentacja techniczna aplikacji Books Manager zrealizowana na potrzeby zaliczenia projektu

Autorzy
--------------
Michał Jurkiewicz oraz Bartłomiej Niewdana.

Biblioteki
--------------
Flask

Spis treści
---------------

.. toctree::
   :maxdepth: 2
   :caption: Root tree

   book
   book_service
   library
   app

