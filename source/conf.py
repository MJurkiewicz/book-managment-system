import sys
import os

sys.path.insert(0, os.path.abspath('..'))
# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Books Manager'
copyright = '2023, Michal Jurkiewicz, Bartosz Niewdana'
author = 'Michal Jurkiewicz, Bartosz Niewdana'
release = 'Version 1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc']

templates_path = ['_templates']
exclude_patterns = []

language = 'pl'

# Add module import paths
sys.path.insert(0, os.path.abspath('../models'))
sys.path.insert(0, os.path.abspath('../handlers'))
sys.path.insert(0, os.path.abspath('../services'))

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'classic'
html_static_path = ['_static']
