from flask import Flask, jsonify, request, render_template, send_from_directory
import os
from handlers.library import Library

app = Flask(__name__, static_folder='web_interface', template_folder='web_interface/templates')

library = Library()


@app.route('/books', methods=['GET'])
def get_all_books():
    """Pobiera wszystkie książki i zwraca stronę z listą książek.

    Returns:
        str: Treść strony HTML z listą wszystkich książek.
    """
    books = library.get_all_books()
    return render_template('all_books.html', books=books)


@app.route('/books/<int:book_id>', methods=['GET'])
def get_book_by_id(book_id):
    """Pobiera informacje o książce o podanym identyfikatorze.

    Args:
        book_id (int): Identyfikator książki.

    Returns:
        dict: Informacje o książce w formacie JSON.
    """
    book = library.get_book(book_id)
    if not book:
        return jsonify({'error': 'Book not found'}), 404
    return jsonify(book)


@app.route('/books', methods=['POST'])
def add_book():
    """Dodaje nową książkę.

    Returns:
        dict: Identyfikator dodanej książki w formacie JSON.
    """
    content_type = request.headers.get('Content-Type')
    if content_type == 'application/json':
        data = request.json
    elif content_type == 'application/x-www-form-urlencoded':
        data = {
            'title': request.form.get('title'),
            'author': request.form.get('author'),
            'year': request.form.get('year')
        }
    else:
        return jsonify({'error': 'Missing data'}), 400

    if not data['title'] or not data['author'] or not data['year']:
        return jsonify({'error': 'Missing data'}), 400

    response = library.add_book(data)
    return jsonify(response), 201


@app.route('/books/<int:book_id>', methods=['PUT'])
def update_book(book_id):
    """Aktualizuje informacje o książce o podanym identyfikatorze.

    Args:
        book_id (int): Identyfikator książki.

    Returns:
        dict: Informacje o zaktualizowanej książce w formacie JSON.
    """
    book = library.get_book(book_id)
    if not book:
        return jsonify({'error': 'Book not found'}), 404

    data = request.get_json()
    updated_book = {
        'title': data.get('title', book['title']),
        'author': data.get('author', book['author']),
        'year': data.get('year', book['year'])
    }

    library.update_book(book_id, updated_book)

    return jsonify({'message': 'Book successfully updated.'}), 200


@app.route('/books/<int:book_id>', methods=['DELETE'])
def delete_book(book_id):
    """Usuwa książkę o podanym identyfikatorze.

    Args:
        book_id (int): Identyfikator książki.

    Returns:
        dict: Informacje o usuniętej książce w formacie JSON.
    """
    deleted_book = library.delete_book(book_id)
    if not deleted_book:
        return jsonify({'error': 'Book not found'}), 404
    else:
        return jsonify(deleted_book), 200


@app.route('/web_interface/images/favicon.ico')
def favicon():
    """Zwraca ikonę favicon.

    Returns:
        file: Ikona favicon.
    """
    return send_from_directory(os.path.join(app.root_path, 'web_interface', 'images'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/', methods=['GET'])
def home():
    """Zwraca stronę główną.

    Returns:
        str: Treść strony HTML z zawartością strony głównej.
    """
    return render_template('index.html')

@app.route('/docs/<path:path>')
def send_sphinx_docs(path):
    return send_from_directory('_build/html', path)


if __name__ == '__main__':
    app.run(debug=True)
