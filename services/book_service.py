import sqlite3
import os


class BookService:
    """Klasa obsługująca operacje na książkach w bazie danych."""

    DATABASE = os.path.join(os.path.dirname(__file__), '../database', 'books.db')

    def __init__(self):
        """Inicjalizuje instancję klasy BookService i tworzy bazę danych, jeśli nie istnieje."""
        self.create_database()

    def create_database(self):
        """Tworzy bazę danych, jeśli nie istnieje."""
        conn = sqlite3.connect(self.DATABASE)
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS books
                    (id INTEGER PRIMARY KEY AUTOINCREMENT,
                    title TEXT NOT NULL,
                    author TEXT NOT NULL,
                    year INTEGER NOT NULL)''')
        conn.commit()
        conn.close()

    def get_all_books(self):
        """Pobiera wszystkie książki z bazy danych.

        Returns:
            list: Lista zawierająca wszystkie książki.
        """
        conn = sqlite3.connect(self.DATABASE)
        c = conn.cursor()
        c.execute("SELECT * FROM books")
        books = c.fetchall()
        conn.close()
        return books

    def get_book(self, book_id):
        """Pobiera książkę o podanym identyfikatorze z bazy danych.

        Args:
            book_id (int): Identyfikator książki.

        Returns:
            tuple: Krotka zawierająca informacje o książce (id, title, author, year).
        """
        conn = sqlite3.connect(self.DATABASE)
        c = conn.cursor()
        c.execute("SELECT * FROM books WHERE id=?", (book_id,))
        book = c.fetchone()
        conn.close()
        return book

    def add_book(self, book):
        """Dodaje książkę do bazy danych.

        Args:
            book (Book): Instancja klasy Book reprezentująca książkę.

        Returns:
            int: Identyfikator dodanej książki.
        """
        conn = sqlite3.connect(self.DATABASE)
        c = conn.cursor()
        c.execute("INSERT INTO books (title, author, year) VALUES(?, ?, ?)",
                  (book.title, book.author, book.year))
        book_id = c.lastrowid
        conn.commit()
        conn.close()
        return book_id

    def update_book(self, book_id, book_data):
        """Aktualizuje informacje o książce o podanym identyfikatorze w bazie danych.

        Args:
            book_id (int): Identyfikator książki.
            book_data (Book): Zaktualizowane informacje o książce.

        Returns:
            None
        """
        conn = sqlite3.connect(self.DATABASE)
        c = conn.cursor()
        c.execute("UPDATE books SET title=?, author=?, year=? WHERE id=?",
                  (book_data.title, book_data.author, book_data.year, book_id))
        conn.commit()
        conn.close()

    def delete_book(self, book_id):
        """Usuwa książkę o podanym identyfikatorze z bazy danych.

        Args:
            book_id (int): Identyfikator książki.

        Returns:
            None
        """
        conn = sqlite3.connect(self.DATABASE)
        c = conn.cursor()
        c.execute("DELETE FROM books WHERE id=?", (book_id,))
        conn.commit()
        conn.close()
