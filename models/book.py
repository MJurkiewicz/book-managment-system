class Book:
    """Klasa reprezentująca książkę."""

    def __init__(self, title: str, author: str, year: int):
        """Inicjalizuje instancję klasy Book.

        Args:
            title (str): Tytuł książki.
            author (str): Autor książki.
            year (int): Rok wydania książki.
        """
        self.title = title
        self.author = author
        self.year = year
