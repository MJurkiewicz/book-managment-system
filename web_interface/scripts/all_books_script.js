function exportToExcel() {

    const table = document.getElementById('booksTable');
    const rows = table.querySelectorAll('tbody tr');


    const data = [['ID', 'Title', 'Author', 'Year']];


    rows.forEach(row => {
      const rowData = [];
      row.querySelectorAll('td').forEach(cell => {
        rowData.push(cell.textContent);
      });
      data.push(rowData);
    });


    const worksheet = XLSX.utils.aoa_to_sheet(data);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Books');


    const excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const excelData = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

    const downloadMessage = document.createElement('div');
    downloadMessage.classList.add('download-message');
    downloadMessage.textContent = 'Downloading...';
    document.body.appendChild(downloadMessage);

    setTimeout(() => {
        document.body.removeChild(downloadMessage);
    }, 2500);


    const excelUrl = URL.createObjectURL(excelData);
    const link = document.createElement('a');
    link.href = excelUrl;
    link.download = 'booksDatabase.xlsx';
    link.click();


    URL.revokeObjectURL(excelUrl);
  }

function goToHomePage() {
  window.location.href = '/';
}

function goToDocumentation() {
  window.location.href = '/docs/index.html';
}


function refreshPage() {
  window.location.reload();
}