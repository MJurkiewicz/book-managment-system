function toggleSection(sectionId) {
  const section = document.getElementById(sectionId);
  section.style.display = section.style.display === 'none' ? 'block' : 'none';
}

function showNotification(message, type) {
  const notification = document.getElementById('notification');
  notification.textContent = message;

  if (type === 'success') {
    notification.className = 'notification success';
  } else if (type === 'error') {
    notification.className = 'notification error';
  } else if (type === 'warning'){
    notification.className = 'notification warning';
  }

  notification.style.display = 'block';

  setTimeout(() => {
    notification.style.display = 'none';
  }, 8000);
}

function addBook() {
  const title = document.getElementById('add-title').value;
  const author = document.getElementById('add-author').value;
  const year = document.getElementById('add-year').value;

  if (!title || !author || !year) {
    showNotification('Missing data', 'error');
    return;
  }

  const data = {
    title: title,
    author: author,
    year: year
  };

  fetch('/books', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(response => {
      if (response.ok) {
        showNotification('Book added successfully', 'success');
        document.getElementById('add-title').value = '';
        document.getElementById('add-author').value = '';
        document.getElementById('add-year').value = '';
      } else {
        showNotification('Failed to add book', 'error');
      }
    })
    .catch(() => {
      showNotification('Failed to add book', 'error');
    });
}

function editBook() {
  const id = document.getElementById('edit-id').value;
  const title = document.getElementById('edit-title').value;
  const author = document.getElementById('edit-author').value;
  const year = document.getElementById('edit-year').value;
  console.log('editBook() called');
  console.log(id, title, author, year);

  if (!id || !title || !author || !year) {
    showNotification('Missing data', 'warning');
    return;
  }

  const data = {
    title: title,
    author: author,
    year: year
  };

  fetch(`/books/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
    .then(response => {
      if (response.ok) {
        showNotification('Book data updated successfully', 'success');
      } else {
        showNotification('Failed to update book data', 'error');
      }
    })
    .catch(() => {
      showNotification('Failed to update book data', 'error');
    });
}

function deleteBook() {
  const id = document.getElementById('delete-id').value;

  if (!id) {
    showNotification('Missing ID', 'error');
    return;
  }

  fetch(`/books/${id}`, {
    method: 'DELETE'
  })
    .then(response => {
      if (response.ok) {
        showNotification('Book deleted successfully', 'success');
      } else {
        showNotification('Failed to delete book', 'error');
      }
    })
    .catch(() => {
      showNotification('Failed to delete book', 'error');
    });
}

function goToLibrary() {
  window.location.href = '/books';
}

function goToDocumentation() {
  window.location.href = '/docs/index.html';
}


function refreshPage() {
  window.location.reload();
}